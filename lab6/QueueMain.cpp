/**
 * This program creates a queue of Civil War officers who agree to a pact 
 * to determine which of them is to go for help because they are surrounded 
 * by Union forces and there is no chance for victory without reinforcements. 
 * They must choose an officer to go for help. The officers form a queue and 
 * they pick a number from a hat.  Beginning with the first officer in the 
 * queue, they begin to count.  As each officer counts off, he moves to the 
 * end of the queue.  When the count reaches the number that was picked, that 
 * officer is removed from the queue, and the count begins again with the 
 * next man.  The last officer that remains in the queue is the one that rides 
 * away on their only horse, Little Sorrel, to summon help.
 *
 */  

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <stdexcept>
#include <cassert>
#include "LinkedQueue.h"
using namespace std;

/**
 * Name of input file containing officer names
 */
const char INPUT_FILE[] = "officers.txt"; 
/**
 * Inputs data from a text file and places each officer's name into the
 * queue passed as a parameter.
 * @param officers - dynamic queue of Confederate officers in the Civil War
 */ 
void loadQueue(LinkedQueue<string> &officers);

/**
 * Displays a list of officers removed from the queue and also displays
 * the name of the officer going for help. Retrieves officers from the 
 * officers queue, one at a time, counting as each is retrieved. If the 
 * count is not equal to hatNumber, the officer is put back into the
 * queue. When the count reaches hatNumber, that officer is removed from
 * the queue permanently (i.e., not put back into the queue), and the
 * officer's name is output to the screen. The last officer remaining in
 * the queue is the one shown as going for help.
 * @param officers - dynamic queue of Confederate officers in the Civil War
 * @param hatNumber - the number drawn from the hat, used to count off
 */ 
void countOff (LinkedQueue<string> officers, int hatNumber);

int main (void)
{
   LinkedQueue<int> intQueue; 
   int value;
   int num;
   int data;
   int count; 

   intQueue.enqueue (35); 
   intQueue.enqueue (51); 
   intQueue.enqueue (18); 
   data = intQueue.peekFront();
   intQueue.dequeue(); 
   intQueue.enqueue (20); 
   value = intQueue.peekFront();
   intQueue.dequeue(); 
   num = intQueue.peekFront();
   intQueue.dequeue();
   intQueue.enqueue (99); 
   count = intQueue.peekFront();
   intQueue.dequeue(); 
   intQueue.enqueue (value); 
   data = intQueue.peekFront();
   intQueue.dequeue(); 
   intQueue.enqueue (num); 
   intQueue.enqueue (79); 
   value = intQueue.peekFront();
   intQueue.dequeue(); 
   data = intQueue.peekFront();
   intQueue.dequeue(); 
   count = intQueue.peekFront();
   intQueue.dequeue(); 
   cout << data << " " << value << " " << num << " " << count << endl;
	return EXIT_SUCCESS;
}


void loadQueue(LinkedQueue<string> &officers)
{
   ifstream inFile(INPUT_FILE); // declare and open the input file
   string officerName; // name of officer input from file

   if (!inFile)
      cout << "Error opening file for input: " << INPUT_FILE << endl;
   else
   {
      getline (inFile, officerName);
      while (!inFile.eof())
      {
         officers.enqueue(officerName);
         getline (inFile, officerName);
      }
      inFile.close();
   }
}


void countOff (LinkedQueue<string> officers, int count)
{
   if(officers.isEmpty())
   {
      cout << "There are no officers to begin with" << endl;
      return;
   }

   // Duplicate queue so algorithm isn't destructive
   LinkedQueue<string> dupOfficers = LinkedQueue<string>(officers);

   string lastOfficer;

   while(dupOfficers.isEmpty() == false)
   {

      // Count 3
      for(int i = 0; i < count-1; i++)
      {
         lastOfficer = dupOfficers.peekFront();
         dupOfficers.dequeue();
         dupOfficers.enqueue(lastOfficer);
      }
         // Remove the 4th count
         lastOfficer = dupOfficers.peekFront();
         dupOfficers.dequeue();

         // Don't print out of last one left
         if(dupOfficers.isEmpty() == false)
         {
            cout << lastOfficer << endl;
         }
   }

   cout << "The officer going for help is " << lastOfficer << endl;
}
