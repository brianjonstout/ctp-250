/** BagTester for Lab 3.
  * @author Professor Gregory
  * @author Brian J Stout
  */
  
#include <iostream>
#include <string>
#include "ArrayBag.h"

using namespace std;

void displayBagOfStrings(ArrayBag<std::string>& bag);
void displayBagOfInts(ArrayBag<int>& bag);
bool testEquiv();
bool testUnion();
bool testIntersection();
bool testDifference();

int main()
{
	bool testResults = true;
	bool retResults = false;

	retResults = testEquiv();
	if(retResults == false)
	{
		testResults = false;
	}

	retResults = testUnion();
	if(retResults == false)
	{
		testResults = false;
	}

	retResults = testIntersection();
	if(retResults == false)
	{
		testResults = false;
	}

	retResults = testDifference();
	if(retResults == false)
	{
		testResults = false;
	}

	if(testResults)
	{
		cout << "Yay!  All the tests passed" << endl;
	}
	else
	{
		cout << "Some tests failed!  What went wrong?" << endl;
	}

   	return 0;
}  

//CODE FROM CLASS
void displayBagOfStrings(ArrayBag<std::string>& bag)
{
	std::cout << "The bag contains " << bag.getCurrentSize()
        << " items:" << std::endl;
   std::vector<std::string> bagItems = bag.toVector();
   
   int numberOfEntries = (int) bagItems.size();
   for (int i = 0; i < numberOfEntries; i++)
   {
      std::cout << bagItems[i] << " ";
   }  
	std::cout << std::endl << std::endl;
}

void displayBagOfInts(ArrayBag<int>& bag)
{
	std::cout << "The bag contains " << bag.getCurrentSize()
        << " items:" << std::endl;
   std::vector<int> bagItems = bag.toVector();
   
   int numberOfEntries = (int) bagItems.size();
   for (int i = 0; i < numberOfEntries; i++)
   {
      std::cout << bagItems[i] << " ";
   }  
	std::cout << std::endl << std::endl;
}
//END CODE FROM CLASS

bool testEquiv()
{
	bool testResult = true;

	cout << "Testing equivalency to self" << endl;
	ArrayBag<int> bag1;
	bag1.add(0);
	bag1.add(1);
	bag1.add(2);
	bag1.add(3);

	if(bag1.bagEquiv(bag1))
	{
		cout << "\tSubtest : Pass" << endl;
	}
	else
	{
		testResult = false;
		cout << "\tSubtest : Fail" << endl;
	}

	cout << "Testing equivalency to other true" << endl;
	ArrayBag<int> bag2;
	bag2.add(0);
	bag2.add(1);
	bag2.add(2);
	bag2.add(3);

	if(bag1.bagEquiv(bag2))
	{
		cout << "\tSubtest : Pass" << endl;
	}
	else
	{
		testResult = false;
		cout << "\tSubtest : Fail" << endl;
	}

	cout << "Testing equivalency to other false" << endl;
	bag2.add(4);

	ArrayBag<int> bag3;
	bag3.add(13);

	ArrayBag<int> emptyBag;


	if(bag1.bagEquiv(bag2) || bag1.bagEquiv(bag3) || bag1.bagEquiv(emptyBag))
	{
		testResult = false;
		cout << "\tSubtest : Fail" << endl;
	}
	else
	{
		cout << "\tSubtest : Pass" << endl;
	}

	return testResult;
	

}

bool testUnion()
{
	bool testResult = true;

	ArrayBag<int> bag1;
	bag1.add(0);
	bag1.add(1);
	bag1.add(2);

	ArrayBag<int> bag2;
	bag1.add(3);
	bag1.add(4);
	bag1.add(5);

	ArrayBag<int> equivBag;
	equivBag.add(0);
	equivBag.add(1);
	equivBag.add(2);
	equivBag.add(3);
	equivBag.add(4);
	equivBag.add(5);

	cout << "Testing Union" << endl;
	if(equivBag.bagEquiv(bag1.bagUnion(bag2)))
	{
		cout << "\tSubtest : Pass" << endl;
	}
	else
	{
		testResult = false;
		cout << "\tSubtest : Fail" << endl;
	}

	return testResult;

}

bool testIntersection()
{
	bool testResult = true;

	ArrayBag<int> bag1;
	bag1.add(0);
	bag1.add(1);
	bag1.add(2);
	bag1.add(3);

	ArrayBag<int> bag2;
	bag2.add(3);
	bag2.add(4);
	bag2.add(5);
	bag2.add(6);

	ArrayBag<int> test1;
	test1.add(3);

	cout << "Testing Base Intersection" << endl;
	if(test1.bagEquiv(bag1.bagIntersection(bag2)))
	{
		cout << "\tSubtest : Pass" << endl;
	}
	else
	{
		testResult = false;
		cout << "\tSubtest : Fail" << endl;
	}

	cout << "Testing Base Intersection (No common)" << endl;
	bag2.remove(3);
	if(!test1.bagEquiv(bag1.bagIntersection(bag2)))
	{
		cout << "\tSubtest : Pass" << endl;
	}
	else
	{
		testResult = false;
		cout << "\tSubtest : Fail" << endl;
	}
	
	bag1.add(3);
	bag1.add(3);
	bag2.add(3);
	bag2.add(3);
	bag2.add(3);
	test1.add(3);

	cout << "Testing multiples" << endl;
	if(!test1.bagEquiv(bag1.bagIntersection(bag2)))
	{
		cout << "\tSubtest : Pass" << endl;
	}
	else
	{
		testResult = false;
		cout << "\tSubtest : Fail" << endl;
	}

	return testResult;
}

bool testDifference()
{
	bool testResult = true;

	ArrayBag<int> bag1;
	bag1.add(0);
	bag1.add(1);
	bag1.add(2);
	bag1.add(3);

	ArrayBag<int> bag2;
	bag2.add(3);
	bag2.add(4);
	bag2.add(5);
	bag2.add(6);

	ArrayBag<int> test1;
	test1.add(0);
	test1.add(1);
	test1.add(2);

	cout << "Testing difference" << endl;
	if(test1.bagEquiv(bag1.bagDifference(bag2)))
	{
		cout << "\tSubtest : Pass" << endl;
	}
	else
	{
		testResult = false;
		cout << "\tSubtest : Fail" << endl;
	}

	test1.add(3);
	ArrayBag<int> emptyBag;
	cout << "Testing difference (empty bag) " << endl;
	if(test1.bagEquiv(bag1.bagDifference(emptyBag)))
	{
		cout << "\tSubtest : Pass" << endl;
	}
	else
	{
		testResult = false;
		cout << "\tSubtest : Fail" << endl;
	}

	return testResult;
}
