// Created by Frank M. Carrano and Timothy M. Henry.
//� Copyright (c) 2017 Pearson Education, Hoboken, New Jersey.

/** Implementation file for the class ArrayBag.
  * Updated to include new methods: bagUnion, bagInterface, and bagDifference.
  * @author Brian J. Stout
  * @author Prof Hall (Code from class)
  * @author Frank M. Carrano
  * @author Timothy M. Henry
  * @file ArrayBag.cpp
  */

#include "ArrayBag.h"
#include <cstddef>
#include <algorithm>
#include <iostream>

using namespace std;

// CODE INCLUDED FROM CLASS
template<class ItemType>
ArrayBag<ItemType>::ArrayBag(): itemCount(0), maxItems(DEFAULT_CAPACITY)
{
}  // end default constructor

template<class ItemType>
int ArrayBag<ItemType>::getCurrentSize() const
{
	return itemCount;
}  // end getCurrentSize

template<class ItemType>
bool ArrayBag<ItemType>::isEmpty() const
{
	return itemCount == 0;
}  // end isEmpty

template<class ItemType>
bool ArrayBag<ItemType>::add(const ItemType& newEntry)
{
	bool hasRoomToAdd = (itemCount < maxItems);
	if (hasRoomToAdd)
	{
		items[itemCount] = newEntry;
		itemCount++;
	}  // end if
    
	return hasRoomToAdd;
}  // end add

/*
// STUB
 template<class ItemType>
 bool ArrayBag<ItemType>::remove(const ItemType& anEntry)
 {
    return false; // STUB
 }  // end remove
*/   
 
template<class ItemType>
bool ArrayBag<ItemType>::remove(const ItemType& anEntry)
{
   int locatedIndex = getIndexOf(anEntry);
	bool canRemoveItem = !isEmpty() && (locatedIndex > -1);
	if (canRemoveItem)
	{
		itemCount--;
		items[locatedIndex] = items[itemCount];
	}  // end if
    
	return canRemoveItem;
}  // end remove

/*
 // STUB
 template<class ItemType>
 void ArrayBag<ItemType>::clear()
 {
    // STUB
 }  // end clear
*/

template<class ItemType>
void ArrayBag<ItemType>::clear()
{
	itemCount = 0;
}  // end clear

template<class ItemType>
int ArrayBag<ItemType>::getFrequencyOf(const ItemType& anEntry) const
{
   int frequency = 0;
   int curIndex = 0;       // Current array index
   while (curIndex < itemCount)
   {
      if (items[curIndex] == anEntry)
      {
         frequency++;
      }  // end if
      
      curIndex++;          // Increment to next entry
   }  // end while
   
   return frequency;
}  // end getFrequencyOf

template<class ItemType>
bool ArrayBag<ItemType>::contains(const ItemType& anEntry) const
{
	return getIndexOf(anEntry) > -1;
}  // end contains

/* ALTERNATE 1: First version
template<class ItemType>
bool ArrayBag<ItemType>::contains(const ItemType& target) const 
{
   return getFrequencyOf(target) > 0;
}  // end contains

// ALTERNATE 2: Second version 
template<class ItemType>
bool ArrayBag<ItemType>::contains(const ItemType& anEntry) const
{
   bool found = false;
   int curIndex = 0;        // Current array index
   while (!found && (curIndex < itemCount))
   {
      if (anEntry == items[curIndex])
      {
         found = true;
      } // end if
      
      curIndex++;           // Increment to next entry
   }  // end while   
   
   return found;
}  // end contains
*/

template<class ItemType>
std::vector<ItemType> ArrayBag<ItemType>::toVector() const
{
   std::vector<ItemType> bagContents;
	for (int i = 0; i < itemCount; i++)
		bagContents.push_back(items[i]);
      
   return bagContents;
}  // end toVector

// private
template<class ItemType>
int ArrayBag<ItemType>::getIndexOf(const ItemType& target) const
{
	bool found = false;
   int result = -1;
   int searchIndex = 0;
   
   // If the bag is empty, itemCount is zero, so loop is skipped
   while (!found && (searchIndex < itemCount))
   {
      if (items[searchIndex] == target)
      {
         found = true;
         result = searchIndex;
      } 
      else
      {
         searchIndex++;
      }  // end if
   }  // end while
   
   return result;
}  // end getIndexOf

// END OF CODE FROM CLASS

/**
 * combines two bags, it treats each number as an individual member, even if they're equivalent
 * @param otherBag The bag being combined with this
 * @return A bag with two combined sets
 */
template<class ItemType>
ArrayBag<ItemType> ArrayBag<ItemType>::bagUnion(const ArrayBag<ItemType> &otherBag) const
{
	ArrayBag<ItemType> retBag;
	
	for(int i = 0; i < this->itemCount; i++)
	{
		bool flag = retBag.add(this->items[i]);
		if(!flag)
		{
			cout << "Bag is full, can't complete union" << endl;
			break;
		}
	}
	
	//TODO: Optimization, single loop
	if(retBag.itemCount > retBag.DEFAULT_CAPACITY)
	{
		return retBag;
	}
	for(int i = 0; i < otherBag.itemCount; i++)
	{
		bool flag = retBag.add(otherBag.items[i]);
		if(!flag)
		{
			cout << "Bag is full, can't complete union" << endl;
			break;
		}
	}
	return retBag;
}

/**
 * creates a new bag with members which were contained in both sets.  If a memeber appears twice in a bag, it'll appear twice in the return bag
 * @param otherBag The bag being intersected with this
 * @return A bag with intersected sets
 */
template<class ItemType>
ArrayBag<ItemType> ArrayBag<ItemType>::bagIntersection(const ArrayBag<ItemType> &otherBag) const
{

	ArrayBag<ItemType> retBag;
	bool takenFlag[otherBag.itemCount] = {false};
	
	for(int i = 0; i < this->itemCount; i++)
	{
		for(int j = 0; j < otherBag.itemCount; j++)
		{
			if(takenFlag[j])
			{
				continue;
			}
			if(items[i] == otherBag.items[j])
			{
				retBag.add(this->items[i]);
				takenFlag[j] = true;
				break;
			}
		}
	}
	
	return retBag;
}

/**
 * Subtracts one bag from another, each memeber of the otherBag set will count once towards This set
 * @param otherBag The bag which will use to subtract
 * @return A bag containg no memebers of the set from otherBag
 */
template<class ItemType>
ArrayBag<ItemType> ArrayBag<ItemType>::bagDifference(const ArrayBag<ItemType> &otherBag) const
{
	ArrayBag<ItemType> retBag(*this); //Clone this arrayBag
	
	//the intersection of this, and otherBag to identify which items need to be removed
	ArrayBag<ItemType> intBag = this->bagIntersection(otherBag);
	
	for(int i = 0; i < retBag.itemCount; i++)
	{
		//TODO: Replace this with contains function for simplification (Same exact thing)
		for(int j = 0; j < intBag.itemCount; j++)
		{
			if(retBag.items[i] == intBag.items[j])
			{
				retBag.remove(retBag.items[i]);
				intBag.remove(intBag.items[j]);

				// Set counters to -1 as iterators will set it to 0 on next loop
				// Reset all the counters over, as remove moves the placement of things
				i = -1;
				j = -1;
				break;
			}
		}
	}
	return retBag;
}

/**
 * returns true if the set is the same size, and contains the same memebers as otherBag
 * @param otherBag The bag being compared to
 * @return A true or false value representing if a bag is equivalent
 */
template<class ItemType>
bool ArrayBag<ItemType>::bagEquiv(const ArrayBag<ItemType> &bag) const
{
	ArrayBag<ItemType> retBag = this->bagDifference(bag);

    if(this->itemCount != bag.itemCount)
    {

        return false;
    }

	if(retBag.itemCount == 0)
	{
		return true;
	}
	else
	{
		return false;
	}	
}

