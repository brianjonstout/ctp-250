#pragma once

#include "ArrayBag.h"
#include <cstddef>
#include <algorithm>
#include <iostream>

using namespace std;

template<class ItemType>
ArrayBag<ItemType> ArrayBag<ItemType>::bagUnion(const ArrayBag<ItemType> &otherBag) const
{
	ArrayBag<ItemType> retBag;
	
	for(int i = 0; i < this->itemCount; i++)
	{
		bool flag = retBag.add(this->items[i]);
		if(!flag)
		{
			cout << "Bag is full, can't complete union" << endl;
			break;
		}
	}
	
	//TODO: Optimization, single loop
	if(retBag.itemCount > retBag.DEFAULT_CAPACITY)
	{
		return retBag;
	}
	for(int i = 0; i < otherBag.itemCount; i++)
	{
		bool flag = retBag.add(otherBag.items[i]);
		if(!flag)
		{
			cout << "Bag is full, can't complete union" << endl;
			break;
		}
	}
	return retBag;
}

template<class ItemType>
ArrayBag<ItemType> ArrayBag<ItemType>::bagIntersection(const ArrayBag<ItemType> &otherBag) const
{

	ArrayBag<ItemType> retBag;
	bool takenFlag[otherBag.itemCount] = {false};
	
	for(int i = 0; i < this->itemCount; i++)
	{
		for(int j = 0; j < otherBag.itemCount; j++)
		{
			if(takenFlag[j])
			{
				continue;
			}
			if(items[i] == otherBag.items[j])
			{
				retBag.add(this->items[i]);
				takenFlag[j] = true;
				break;
			}
		}
	}
	
	return retBag;
}

template<class ItemType>
ArrayBag<ItemType> ArrayBag<ItemType>::bagDifference(const ArrayBag<ItemType> &otherBag) const
{
	ArrayBag<ItemType> retBag(*this); //Clone this arrayBag
	
	//the intersection of this, and otherBag to identify which items need to be removed
	ArrayBag<ItemType> intBag = this->bagIntersection(otherBag);
	
	for(int i = 0; i < retBag.itemCount; i++)
	{
		//TODO: Replace this with contains function for simplification (Same exact thing)
		for(int j = 0; j < intBag.itemCount; j++)
		{
			if(retBag.items[i] == intBag.items[j])
			{
				retBag.remove(retBag.items[i]);
				intBag.remove(intBag.items[j]);

				// Set counters to -1 as iterators will set it to 0 on next loop
				// Reset all the counters over, as remove moves the placement of things
				i = -1;
				j = -1;
				break;
			}
		}
	}
	return retBag;
}

template<class ItemType>
bool ArrayBag<ItemType>::bagEquiv(const ArrayBag<ItemType> &bag) const
{
    cout << "ATTENTION" << this->itemCount << " " << bag.itemCount << endl;
	ArrayBag<ItemType> retBag = this->bagDifference(bag);

    if(this->itemCount != bag.ItemCount)
    {

        return false;
    }

	if(retBag.itemCount == 0)
	{
		return true;
	}
	else
	{
		return false;
	}	
}