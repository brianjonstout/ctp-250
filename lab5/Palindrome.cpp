/**
 * @file Palindrome.cpp
 * @author Brian J. Stout + code from class
 * @date 10MAR20
 */
#include "Palindrome.h"
#include <iostream>
using namespace std;

Palindrome::Palindrome () 
{
   phrase = "";
}


Palindrome::Palindrome (string newPhrase) 
{
   phrase = newPhrase;
}
   

// After completing this method, submit the entire .cpp
// file as your solution for Lab 5. See method description in 
// Palindrome.h

/**
* Takes an address to the string, reverses it using a stack, and then compares the reversed phrase
*  to the original phrase to determine if it is a palindrome
*
* @param reversePhrase the address of a string which will contain the reversedPhrase afterwards
* @return If the phrase is a palindrome it will return true.  If the phrase is not false.
*/
bool Palindrome::evalPalindrome (string& reversePhrase)
{

   if(phrase.length() < 2)
   {
      return false;
   }

   //Place code for lab here
   ArrayStack<char> charStack;

   for(int i = 0; i < phrase.length(); i++)
   {
      charStack.push(phrase.at(i));
   }

   //Reverse the phrase with the stack
   while(charStack.isEmpty() == false)
   {
      reversePhrase += charStack.peek();
      charStack.pop();
   }

   char palinLetter = 0;
   char reverseLetter = 0;

   int matchCount = 0; //Keeps track of how many alphanum matches there were

   for(int i = 0, j = 0; i < phrase.length() && j < phrase.length(); i++, j++)
   {
      palinLetter = phrase.at(i);
      reverseLetter = reversePhrase.at(j);

      if(isalnum(palinLetter) && isalnum(reverseLetter))
      {
         //If they aren't they same, it's not a palindrome
         if(tolower(palinLetter) != tolower(reverseLetter))
         {
            return false;
         }
         matchCount++;
      }
      else
      {
         //Ignores punctuation and non-valid comparisons
         if(isalnum(palinLetter) == false)
         {
            i++;
         }
         if(isalnum(reverseLetter) == false)
         {
            j++;
         }
      }
   }

   //Only return true if there were two valid alphanumeric comparisons
   if(matchCount > 1)
   {
      return true;
   }
   else
   {
      return false;
   }
}
