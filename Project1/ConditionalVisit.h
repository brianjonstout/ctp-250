/**
 * @file ConditionalVisit.h
 * @author Brian Stout
 * @brief Extends BinaryTreeVisitor.h allowing to store a Film as what's being reported, and setting a condition (to make comparison)
 *  and prevent further traversal of the BST
 * @version 1.0
 * @date 2020-04-28
 * 
 */

#pragma once
#ifndef CONDITIONALVISIT_H
#define CONDITIONALVISIT_H

#include <iostream>
#include "BinaryTreeVisitor.h"
#include "Film.h"

using namespace std;

template<typename ItemType>
class ConditionalVisit : public BinaryTreeVisitor
{
    private:

    public:

        Film report;
        ItemType cond;
        bool found = false;

    //TODO: Figure out how templates work and move implementation outside header
    // https://stackoverflow.com/a/10632266

    /**
     * @brief Get the Found object
     * 
     * @return if an object has been found returns true
     */
    bool getFound()
    {
        return found;
    }

    /**
     * @brief Set the Found object
     * 
     * @param val 
     */
    void setFound(bool val)
    {
        found = val;
    }

    /**
     * @brief Get the Cond object
     * 
     * @return ItemType get the value being compared for the condition to be met
     */
    ItemType getCond()
    {
        return cond;
    }

    /**
     * @brief Set the Cond object
     * 
     * @param val 
     */
    void setCond(ItemType val)
    {
        cond = val;
    }

    /**
     * @brief Get the Report object
     * 
     * @return Film 
     */
    Film getReport()
    {
        return report;
    }

    /**
     * @brief Set the Report object
     * 
     * @param film the film which needs to be displayed outside of the visit function
     */
    void setReport(Film film)
    {
        report = film;
    }

    /**
     * @brief Required to be implemented here since BinaryTreeVisitor.h has a virtual implementation of it, classes which extend this one should implement their own method
     * 
     * @param film the film being visited
     */
    void visit(Film& film)
    {
    }
};



#endif