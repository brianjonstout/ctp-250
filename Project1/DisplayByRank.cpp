#include "DisplayByRank.h"

using namespace std;

void DisplayByRank::visit(Film& film)
{
    if(this->getCond() == film.getRank()) //If rank is same as specified set the report to the current film
    {
        this->setReport(film);
        this->setFound(true);
    }
}
