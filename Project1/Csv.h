/**
 * @file Csv.h
 * @author Brian Stout
 * @brief Takes a string and delivers each string between commas
 * @version 1.0
 * @date 2020-04-28
 * 
 * 
 */

#pragma once

#include <iostream>
using namespace std;

/**
 * @brief When given a string, getNext() will populate a string address with each
 *  value between commas.
 * 
 */
class Csv {

private:
    string record;
    size_t lastPos = 0;

public:

    /**
     * @brief Construct a new Csv object with a string already
     * 
     */
    Csv(string);

    /**
     * @brief Default deconstructor
     * 
     */
    Csv();
    void setString(string);
    string getString();

    /** 
     *  Places each value in a CSV record into a string address in the order they appear
     *  @return bool which represents if a new value was placed into val
     *  @param val a string object to place the value into
     */
    bool getNext(string& val);

};
