/**
 * @file SearchByStudio.h
 * @author Brian Stout
 * @brief Prints out any film with a matching studio
 * @version 1.0
 * @date 2020-04-28
 * 
 */
#pragma once

#include <iostream>
#include "ConditionalVisit.h"
#include "Utils.h"
#include "FilmDatabase.h"

using namespace std;

/**
 * @brief Prints out any film with a matching studio
 * 
 */
class SearchByStudio: public ConditionalVisit<string>
{
    public:
        void visit(Film&);

};