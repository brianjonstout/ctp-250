#include <cstdlib>
#include "MenuDBTask.h"

MenuDBTask::MenuDBTask(MenuDBOpFPtr opToDo, FilmDatabase* film)
{
    this->opToDo = opToDo;
    this->filmDB = film;
}

MenuDBTask::~MenuDBTask() {
}

int MenuDBTask::run(void) {   
    return (*opToDo)(this->filmDB);    
}