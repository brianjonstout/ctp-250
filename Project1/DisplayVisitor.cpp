#include "DisplayVisitor.h"

using namespace std;

void DisplayVisitor::visit(Film& film)
{
    cout << film << endl;
}