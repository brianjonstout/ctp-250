/**
 * @file DisplayByRank.h
 * @author Brian Stout
 * @brief An implementation of ConditionalVisit where the condition is an int type.  Used to compare film ranks
 * @version 1.0
 * @date 2020-04-28
 * 
 * 
 */
#pragma once

#include <iostream>
#include "ConditionalVisit.h"

using namespace std;

/**
 * @brief An implementation of ConditionalVisit where the condition is an int type.  Used to compare film ranks
 * 
 */
class DisplayByRank: public ConditionalVisit<int>
{

    public:
        /**
         * @brief What is done to each node visited
         * 
         */
        void visit(Film&);

};