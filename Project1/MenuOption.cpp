/* 
 * @File:   MenuOperation.cpp
 * Author: Daniel Hall
 *
 * Created on April 4, 2017, 11:45 AM
 */

#include "MenuOption.h"


MenuOption::MenuOption(const char key, const string& label, MenuOperation* mOp) {
    this->key = key;
    this->label = label;
    this->mOp = mOp;
}


MenuOperation* MenuOption::getOperation() {
    return this->mOp;
}


bool MenuOption::operator==(const char& rightHandSide) {
    return this->key == rightHandSide;    
}

const string MenuOption::getLabel() {
    return this->label;
}

const char MenuOption::getKey() {
    return this->key;
}

MenuOption::~MenuOption() {
    
}
