/**
 * @file SearchByDate.h
 * @author Brian Stout
 * @brief A conditional visit which compares dates to each node visited
 * @version 1.0
 * @date 2020-04-28
 * 
 * 
 */
#pragma once

#include <iostream>
#include "ConditionalVisit.h"
#include "Utils.h"
#include "FilmDatabase.h"

using namespace std;

/**
 * @brief Will print out any Film with a date which matches the set condition
 * 
 */
class SearchByDate: public ConditionalVisit<int>
{
    public:
        void visit(Film&);

};