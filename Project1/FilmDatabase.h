/**
 * @file FilmDatabase.h
 * @author Brian Stout
 * @brief Contains the FilmDatabase class which contains a BinarySearchTree<Film> object and all the related methods
 * @version 1.0
 * @date 2020-04-28
 * 
 * 
 */

#pragma once

#include <iostream>
#include "BinarySearchTree.h"
#include "DisplayByRank.h"
#include "DisplayVisitor.h"
#include "SearchByTitle.h"
#include "SearchByKeyword.h"
#include "SearchByStudio.h"
#include "SearchByDate.h"
#include "Film.h"
#include "DisplayVisitor.h"
#include "Utils.h"
#include "Csv.h"

using namespace std;

class FilmDatabase {

private:

    BinarySearchTree<Film> filmDatabaseBST;

public:

    /**
     * @brief add a film to the BST
     * 
     * @return true if added
     * @return false if not
     */
    bool add(Film&);

    /**
     * @brief remove a film from the bst
     * 
     * @return true if removed
     * @return false if not
     */
    bool remove(Film&);

    /**
     * @brief Displays the films inorder.  Since the key is the Film title it wil be displayed
     *  alphabetically, spaces are not removed so will count for the string comparisons
     * 
     */
    void displayData();

    /**
     * @brief Displays the films in order by rank
     * 
     */
    void displayByRank();

    /**
     * @brief Helper function, just runs displayData() as the key is by title
     * 
     */
    void displayByTitle();

    /**
     * @brief Searches the BST for a specific move title and displays it
     * 
     */
    void searchByTitle(string);

    /**
     * @brief Searches the BST for films which contains specified keywords and displays them
     * 
     */
    void searchByKeyword(string);

    /**
     * @brief Searches the BST for films made by a specified studio and displays them
     * 
     */
    void searchByStudio(string);

    /**
     * @brief Searches the BST for films released in a specified month
     * 
     */
    void searchByDate(int);

    /**
     * @brief Returns the number of films in the BST
     * 
     * @return int 
     */
    int getNumberOfFilms();

};
