#include "SearchByStudio.h"

using namespace std;

void SearchByStudio::visit(Film& film)
{
    string studioLower = film.getStudio(); 
    Utils::lowerString(studioLower);

    if(this->getCond() == studioLower)
    {
        if(this->getFound() == false)
        {
            film.printheader();
        }

        cout << film << endl;
        this->setFound(true);


    }
}