/* 
 * @File:   MenuOptionTest.cpp
 * Author: Daniel Hall
 *
 * Created on April 4, 2017, 11:45 AM
 * 
 * 
 * Test program used to develop and test the MenuOption and MenuTask classes.
 */


#include <cstdlib>
#include <iostream>

#include "MenuOption.h"
#include "MenuTask.h"
#include "MenuOption.h"
#include "MenuTask.h"

using namespace std;


int moAssignedTask1();
int moAssignedTask2();



int main() {
    MenuOperation* mopx;
    

    MenuTask mop1(moAssignedTask1);
    string mo1Label = "Do Assigned Task 1";
    MenuOption mo1('T', mo1Label, &mop1);    
    mopx = mo1.getOperation();    
    mopx->run();
    
    MenuTask mop2(moAssignedTask2);
    string mo2Label = "Do Assigned Task 2";
    MenuOption mo2('T', mo2Label, &mop2);    
    mopx = mo2.getOperation();    
    mopx->run();
    
}


int moAssignedTask1() {
    cout << "moAssignedTask1()" << endl;
    return 0;
}

int moAssignedTask2() {
    cout << "moAssignedTask2()" << endl;
    return 0;
}