/* 
 * @File:   MenuKeyDupliatedException.cpp
 * @Author: Daniel Hall
 *
 * Created on April 8, 2017, 1:12 PM
 */

#ifndef _MENU_KEY_DUPLICATED_EXCEPTION
#define _MENU_KEY_DUPLICATED_EXCEPTION

#include <stdexcept>
#include <string>

using namespace std;

/**
 * Exception class that gets created if code using the Menu class attempts to 
 * add an option using a key that is already in use.
 */
class MenuKeyDuplicatedException : public logic_error
{
public:
    
   /**
    * Constructor for exception.
    * @param message
    */   
   MenuKeyDuplicatedException(const string& message = "");
}; // end _MENU_KEY_DUPLICATED_EXCEPTION 
#endif
