/*
 * File:   Menu.cpp
 * Author: Daniel
 *
 * Created on April 3, 2017, 12:35 PM
 * 
 * The Menu class is used to embody a hierarchical menuing system. 
 * 
 */



#include <stdio.h>
#include "Menu.h"
#include "PromptUtils.h"




Menu::Menu() {
    
}


Menu::Menu(const string title, const string prompt) {
    this->title = title;
    this->prompt = prompt;
    options.reserve(20);
    this->vSize = 0;
}


void Menu::addOption(MenuOption* mo)  throw(MenuKeyDuplicatedException) {
    
    // We need to make sure that the value for mo.key isn't already taken.
    // If it is, we need to throw an exception.
    for (int i = 0; i < options.size(); i++) {
	MenuOption *mox = options[i];
	if (mox->getKey() == mo->getKey()) {
	    char text[100];
	    sprintf(text, "Duplicate menu key in same menu: <%c>.",mo->getKey());
	    string errorMsg(text);
	    throw MenuKeyDuplicatedException(errorMsg);
	}	
    }
  
    options.push_back(mo);
    vSize++;
}



int Menu::run() {

//    cout << __FILE__ << ":" << __LINE__ << " : Menu::run() - " << this->title << endl;

    MenuOperation* mOp;
    int menuCtl = MENU_SAME;

    do {
//	cout << __FILE__ << ":" << __LINE__ << " : " << endl;	
	mOp = displayMenu();

//	cout << "Calling mOp->run()..." << endl;
	menuCtl = mOp->run();
//	cout << "Back from mOp->run()..." << endl;

    } while (menuCtl == MENU_SAME);

    return MENU_SAME;
}



MenuOperation* Menu::displayMenu() {
    
//    cout << __FILE__ << ":" << __LINE__ << " : Menu::displayMenu() - " << endl;

    vector<char> vOptKeys;
    MenuOption* moPtr;
    cout << endl << this->title << endl;

    for (int i = 0; i < options.size(); i++) {
//	cout << __FILE__ << ":" << __LINE__ << " : Menu::displayMenu() - " << endl;
	moPtr = options[i];
	cout << " " << moPtr->getKey() << ". " << moPtr->getLabel() << endl;
	vOptKeys.push_back(moPtr->getKey());
    }
//    cout << __FILE__ << ":" << __LINE__ << " : Menu::displayMenu() - " << endl;
    char selectedOptChar = promptForCharInSet(this->prompt, this->vSize, vOptKeys);

    MenuOption* moSelected = getOptionForChar(selectedOptChar);

//    cout << __FILE__ << ":" << __LINE__ << " : Menu::displayMenu() - " << endl;
    return moSelected->getOperation();

}


MenuOption* Menu::getOptionForChar(const char moCh)
{
//    cout << __FILE__ << ":" << __LINE__ << " : Menu::getOptionForChar() - " << endl;
    MenuOption* moPtr;


    for (int i = 0; i < options.size(); i++) {
	moPtr = options[i];
	if (moCh == moPtr->getKey()) {
//	    cout << __FILE__ << ":" << __LINE__ << " : Menu::getOptionForChar() - returning moPtr" << endl;
	    return moPtr;
	}
    }
//    cout << __FILE__ << ":" << __LINE__ << " : Menu::getOptionForChar() - returning NULL" << endl;
    return NULL;
    // Throw exception if moCh not found.
//    throw NotFoundException("MenuOption corresponding to key('" + moCh + "') not found.");
}


Menu::~Menu() {

}