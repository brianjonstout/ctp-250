/**
 * @file Film.h
 * @author Brian Stout
 * @brief A class which when given a CSV string formatted correctly will populate and store data for a released film
 * @version 1.0
 * @date 2020-04-28
 * 
 * 
 */

#pragma once

#include <iostream>
#include <iomanip>
#include <string>
#include <ctime>
#include "Csv.h"
#include "Utils.h"
#include "FormatAsUSDollar.h"


using namespace std;

class Film {

private:
    int rank;
    string filmTitle;
    string studio;
    double totalGross;
    int totalTheaters;
    double openingGross;
    int openingTheaters;
    string openingDate;
    string formattedDate;
    int month;

    /**
     * @brief Helper function which runs a string through a Csv object and places each value in the appropriate field
     * 
     * @return true if film populated correctly
     * @return false if film did not populate correctly
     */
    bool populateFilm(string);
    bool formatDate(string);

public:
    /**
     * @brief Construct a new Film object
     * 
     */
    Film();

    /**
     * @brief Construct a new Film object
     * 
     */
    Film(string);

    /**
     * @brief Get the Rank object
     * 
     * @return int 
     */
    int getRank();

    /**
     * @brief Get the Film Title
     * 
     * @return string 
     */
    string getFilmTitle();

    /**
     * @brief Get the Studio
     * 
     * @return string 
     */
    string getStudio();

    /**
     * @brief Get the Total Gross
     * 
     * @return double 
     */
    double getTotalGross();

    /**
     * @brief Get the Total Theaters it showed in
     * 
     * @return int 
     */
    int getTotalTheaters();

    /**
     * @brief Get the Opening Gross of the film
     * 
     * @return double 
     */
    double getOpeningGross();

    /**
     * @brief Get the Opening Theaters it showed in
     * 
     * @return int 
     */
    int getOpeningTheaters();

    /**
     * @brief Get the Opening Date
     * 
     * @return string 
     */
    string getOpeningDate();

    /**
     * @brief Get the Month
     * 
     * @return int 
     */
    int getMonth();

    /**
     * @brief print out the film
     * 
     */
    void displayFilm();

    /**
     * @brief Print the header which a printed film will fit under
     * 
     */
    static void printheader();

    friend ostream& operator<<(ostream&, const Film&);
    friend bool operator>(const Film&, const Film&);
    friend bool operator>=(const Film&, const Film&);
    friend bool operator<(const Film&, const Film&);
    friend bool operator<=(const Film&, const Film&);
    friend bool operator==(const Film&, const Film&);
};
