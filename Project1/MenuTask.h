/* 
 * @File:   MenuTask.h
 * @Author: Daniel Hall
 *
 * Created on April 4, 2017, 11:45 AM
 */

#ifndef MENUTASK_H
#define MENUTASK_H

#include "MenuOperation.h"

/*
 * typedef necessary to allow passing MenuOperation function pointer.
 */
typedef int (*MenuOpFPtr)();


/**
 * MenuTask is used to embody desired functionality with a Menu.
 *
 */
class MenuTask : public MenuOperation
{
private:
    int (*opToDo)();

public:
    /**
     * Constructor used to create a MenuTask with a particular function as the
     * the functionality to be performed when accessed.
     * 
     * @param opToDo
     */
    MenuTask(MenuOpFPtr opToDo); 
    
    /**
     * Default destructor.
     */
    ~MenuTask();
    
    /**
     * The run method will be called when the user selects the the MenuOption
     * that contains this MenuTask.
     * 
     * @return int success or failure.
     */
    int run(void);
};


#endif /* MENUTASK_H */

