#include "Film.h"


bool Film::populateFilm(string record)
{
    Csv csv(record);
    string val = "";

    //TODO: Catch invalid_argument exception
    //TODO: Throw invalid_argument exception if record can't populate every field

    //Grab rank value
    bool success = csv.getNext(val);
    if(success)
    {
        this->rank = stoi(val, nullptr, 10);
    }
    else
    {
        //Field couldn't be populated becaused record is too short
        cout << "ERROR: csv record \"" << record << "\" doesn't contain enough values" << endl;
        return false;
    }

    //Grab filmTitle value
    success = csv.getNext(val);
    if(success)
    {
        this->filmTitle = val;
    }
    else
    {
        //Field couldn't be populated becaused record is too short
        cout << "ERROR: csv record \"" << record << "\" doesn't contain enough values" << endl;
        return false;
    }

    //Grab studio value
    success = csv.getNext(val);
    if(success)
    {
        this->studio = val;
    }
    else
    {
        //Field couldn't be populated becaused record is too short
        cout << "ERROR: csv record \"" << record << "\" doesn't contain enough values" << endl;
        return false;
    }

    //Grab totalGross
    success = csv.getNext(val);
    if(success)
    {
        this->totalGross = stod(val);
    }
    else
    {
        //Field couldn't be populated becaused record is too short
        cout << "ERROR: csv record \"" << record << "\" doesn't contain enough values" << endl;
        return false;
    }

    //Grab totaltheaters value
    success = csv.getNext(val);
    if(success)
    {
        this->totalTheaters = stoi(val, nullptr, 10);
    }
    else
    {
        //Field couldn't be populated becaused record is too short
        cout << "ERROR: csv record \"" << record << "\" doesn't contain enough values" << endl;
        return false;
    }

    //Grab openingGross value
    success = csv.getNext(val);
    if(success)
    {
        this->openingGross = stod(val, nullptr);
    }
    else
    {
        //Field couldn't be populated becaused record is too short
        cout << "ERROR: csv record \"" << record << "\" doesn't contain enough values" << endl;
        return false;
    }

    //Grab openingTheaters value
    success = csv.getNext(val);
    if(success)
    {
        this->openingTheaters = stoi(val, nullptr, 10);
    }
    else
    {
        //Field couldn't be populated becaused record is too short
        cout << "ERROR: csv record \"" << record << "\" doesn't contain enough values" << endl;
        return false;
    }

    //Grab openingDate value
    success = csv.getNext(val);
    if(success)
    {
        openingDate = val;
        bool result = formatDate(openingDate);
    }
    else
    {
        //Field couldn't be populated becaused record is too short
        cout << "ERROR: csv record \"" << record << "\" doesn't contain enough values" << endl;
        return false;
    }

    return true;
}

bool Film::formatDate(string date)
{
    //If the date format is dd-MON
    if(date.find('-'))
    {
        string month = date.substr(date.find('-')+1, date.length());
        this->month = Utils::monthLetterToInt(month);
        formattedDate = date;
    }
    //If the format is mm/dd/YY
    else
    {
        size_t firstSlash = date.find('/');
        string month = date.substr(0, firstSlash);
        date.erase(0, firstSlash + 1);
        string day = date.substr(0, date.find('/'));

        this->month = stoi(month);
        formattedDate = Utils::dateformat(stoi(day), stoi(month));
    }
    return false;
}

Film::Film()
{

}

Film::Film(string record)
{
    bool result = populateFilm(record);
    if(result == false)
    {
        //TODO throw exception
    }
}

int Film::getRank()
{
    return this->rank;
}

string Film::getFilmTitle()
{
    return this->filmTitle;
}

string Film::getStudio()
{
    return this->studio;
}

double Film::getTotalGross()
{
    return this->totalGross;
}

int Film::getTotalTheaters()
{
    return this->totalTheaters;
}

double Film::getOpeningGross()
{
    return this->openingGross;
}

int Film::getOpeningTheaters()
{
    return this->openingTheaters;
}

string Film::getOpeningDate()
{
    return this->openingDate;
}

int Film::getMonth()
{
    return this->month;
}

void Film::printheader()
{
    //TODO No magic numbers here for formatting
    cout << setfill(' ');

    cout << setw(95) << "Total" 
    << setw(10) << "Total" << setw(20) << "Opening" << setw(20) << "Opening"
    << setw(10) << "Opening" << endl;

    cout << setw(4) << "Rank" << setw(6) << "Title" << setw(60) << "Studio"
    << setw(25) << "Gross" << setw(10) << "Theaters" << setw(20) << "Gross"
    << setw(20) << "Theaters" << setw(10) << "Date" << endl;

    cout << setfill('-');
    cout << setw(5) << " " << setw(56) << " " << setw(10) << " " << setw(25) << " "
    << setw(10) << " " << setw(20) << " " << setw(20) << " "
    << setw(10) << " " << endl;
}

ostream& operator<<(ostream& os, const Film& film)
{
    double totalGross = film.totalGross;
    double openingGross = film.openingGross;

    os << setfill(' ') << setw(4) << film.rank << " " << setw(55) << film.filmTitle
    << setw(10) << film.studio << setw(10) << " " << Format::formatAsUSMoney(totalGross)
    << setw(10) << film.totalTheaters << setw(20)
    << Format::formatAsUSMoney(openingGross)  << setw(19)
    << film.openingTheaters  << setw(10) << film.formattedDate;
    return os;
}

bool operator>(const Film& lhs, const Film& rhs)
{
    return lhs.filmTitle > rhs.filmTitle;
}

bool operator>=(const Film& lhs, const Film& rhs)
{
    return lhs.filmTitle >= rhs.filmTitle;
}

bool operator<(const Film& lhs, const Film& rhs)
{
    return lhs.filmTitle < rhs.filmTitle;
}

bool operator<=(const Film& lhs, const Film& rhs)
{
    return lhs.filmTitle <= rhs.filmTitle;
}

bool operator==(const Film& lhs, const Film& rhs)
{
    return lhs.filmTitle == rhs.filmTitle;
}