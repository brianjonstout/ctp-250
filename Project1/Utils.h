/**
 * @file Utils.h
 * @author Brian Stout
 * @brief Contains utility functions used in the project
 * @version 1.0
 * @date 2020-04-28
 * 
 */
#pragma once

#include <string>

using namespace std;

namespace Utils
{
    /**
     * @brief Lowers all captial letters to lowercase
     * 
     */
    void lowerString(string&);

    /**
     * @brief Converts the date format found in Films2015.csv to those found in Films2016.csv
     * 
     * @return string
     */
    string dateformat(int, int);

    /**
     * @brief Given a abbreivated month "Jul" will return it to the required int
     * 
     * @return int representing the month (1 is Jan)
     */
    int monthLetterToInt(string);
}
