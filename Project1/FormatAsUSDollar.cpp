/* 
 * File:   FormatAsUSDollar.cpp
 * Author: Daniel Hall
 *
 * Created on April 11, 2017, 12:29 PM
 * Modified by Brian Stout on April 28, 2020
 * Modifications : Used a namespace and #pragma once to avoid redefinitions
 */
#include <iostream>
#include "FormatAsUSDollar.h"

using namespace std;

string Format::formatAsUSMoney(double d) {
    
    char buffer[20];
    
    sprintf(buffer, "%10.2f", d);
    
    char dollars[11];
    char cents[3];
    
    char *to = dollars;
    char *from = buffer;
    
    // Skip leading whitespace.
    while (isspace(*from)) {
	from++;
    }
    
    while (*from != '.' && *from != (char)NULL) {
//	cout << "Copy Dollar Digit: [" << *from << "]" << endl;
	*to++ = *from++;
    }
    if (*from == '.') {
	*to = (char)NULL;
	to = cents;
    }
    
    
    from++;
    
    while (*from != (char)NULL) {
	*to++ = *from++;
    }
    *to = (char)NULL;
    
    string sDollarsTemp(dollars);
    string sCents(cents);
        
    string sDollars;
    string formatted;
    
    formatted.append("$ ");
    
    if (sDollarsTemp.length() > 3) {
	int sizeOfFirstChunk = sDollarsTemp.length() % 3;
	
	int chunkCount = sDollarsTemp.length() / 3;	    // integer division
	
	if (sizeOfFirstChunk == 0) {
	    sizeOfFirstChunk = 3;
	    chunkCount--;
	}	
	
	string firstChunk = sDollarsTemp.substr(0, sizeOfFirstChunk);
	
	formatted.append(firstChunk);
	
	for (int i = 0; i < chunkCount; i++) {
	    formatted.append(",");
	    int begin = sizeOfFirstChunk+(i*3);
	    int len = 3;
	    string chunk = sDollarsTemp.substr(begin,len);
	    formatted.append(chunk);
	}
	formatted.append(".");
	formatted.append(sCents);
	
    } else {
	formatted.append(sDollarsTemp);
	formatted.append(".");
	formatted.append(sCents);
    }
    
    
    return formatted;
}