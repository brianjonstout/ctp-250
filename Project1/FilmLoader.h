/* 
 * File:   FilmLoader.h
 * Author: Daniel Hall, Brian Stout (Modified)
 *
 * Created on April 8, 2017, 1:12 PM
 */

#ifndef FILMLOADER_H
#define FILMLOADER_H

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <stdexcept>
#include <cassert>
#include "Film.h"
#include "NumberFormatException.h"

using namespace std;

/**
 * The FilmLoader opens and provides a "getNext(Film& film)" functionality, 
 * making it easier to load the csv film data file.
 * 
 */
class FilmLoader {
    
private:
    
    //Changed from char* to string because it's easier to work with
    string inFileName;
    
    std::ifstream reader;
    
    //Commented out as the Csv class handles this functionality
    //bool getNextField(char **c, char[]);
    
public:
    /**
     * Constructor that open the csvFile.
     * @param csvFileName
     */
    FilmLoader(const char* csvFileName);
    
    /**
     * Default destructor.
     */
    ~FilmLoader();
    
    /**
     * Method is used to put the next line of data from the file and convert
     * the csv line data into a Film object.
     * 
     * @param film& The film object that is produced to contain the csv line 
     * data.
     * @return boolean to indicate the success or failure of the operation.
     */
    bool getNext(Film& film); //TODO: throw(NumberFormatException);    
    
};
#endif /* FILMLOADER_H */

