/**
 * @file DisplayVisitor.h
 * @author Brian Stout
 * @brief An implementation of BinaryTreeVisitor which prints out each film visited
 * @version 1.0
 * @date 2020-04-28
 * 
 * 
 */
#ifndef DisplayVisitor_h
#define DisplayVisitor_h

#include <iostream>
#include "BinaryTreeVisitor.h"

using namespace std;

class DisplayVisitor: public BinaryTreeVisitor
{
    public:
        /**
         * @brief Prints out the film it visits
         * 
         * @param film the film being printed out
         */
        void visit(Film& film);
};



#endif // FilmDatabase_h