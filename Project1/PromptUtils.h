#pragma once //This not being header guarded caused some issues

/*
 * @File:	PromptUtils.h
 * @Author:	Daniel Hall
 * Course:	CTP-250
 * 
 * Header file for PromptUtils.cpp
 * 
 * Created on January 22, 2017, 5:49 PM
 *  
 */
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <cctype>
#include <sstream>
#include <ctype.h>
#include "string.h"

#include <string>
#include <vector>

using namespace std;

/**
 * Function implements a controlled prompting capability using a prompt and 
 * a description provided by the caller. The caller also provides a int
 * min and max values to ensure that returned value is in the desired range.
 * 
 * @param prompt The initial prompt text presented to the user.
 * @param description A description presented to the use if they enter an 
 * invalid value at the first prompt.
 * @param min The minimum value accepted. If min is -1, no minium is enforced.
 * @param max The maximum value accepted. If max is -1, no maximum is enforced.
 * @return The valdated int.
 */
int promptForPositiveInt(string prompt, string description, int min, int max);

/**
 * Function implements a controlled prompting capability using a prompt 
 * provided by the caller to prompt the user for a 'y' or 'n'. 
 * 
 * @param prompt The initial prompt presented to the caller.
 * @return a char containing 'Y' or 'N' reflecting the user's input.
 */
char promptForYN(string prompt);

/**
 * Function implements a controlled prompting capability using a prompt 
 * provided by the caller to prompt the user for a character that exists
 * within a set of characters.
 * 
 * @param prompt The initial prompt presented to the caller.
 * @return a char containing 'Y' or 'N' reflecting the user's input.
 */
char promptForCharInSet(const string prompt, const int vSize, const vector<char> allowableChars);

/**
 * A check function to verify that s contains only digits.
 * 
 * @param s A string to be examined.
 * @return True if s contains only digits, false otherwise.
 */
bool pfpiIsInt(const string s);