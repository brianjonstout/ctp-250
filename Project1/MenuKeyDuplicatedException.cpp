//  Created by Daniel Hall.
/** @file MenuKeyDuplicatedException.cpp */
#include "MenuKeyDuplicatedException.h"  

MenuKeyDuplicatedException::MenuKeyDuplicatedException(const string& message)
         : logic_error("Duplicate Menu Key: " + message)
{
}  // end constructor

// End of implementation file.

