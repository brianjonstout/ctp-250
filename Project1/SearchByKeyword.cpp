#include "SearchByKeyword.h"

using namespace std;

void SearchByKeyword::visit(Film& film)
{
    vector<string> words = this->getCond();

    string titleLower = film.getFilmTitle(); 
    Utils::lowerString(titleLower);

    //Loops through each keyword
    for(auto i: words)
    {
        size_t result = titleLower.find(i);
        if(result != string::npos)
        {
            //Only print out header once
            if(this->getFound() == false)
            {
                film.printheader();
                this->setFound(true);
            }
            cout << film << endl;
            break;
        }
    }
}