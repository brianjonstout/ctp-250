/*
 * File:   Menu.h
 * Author: Daniel
 *
 * Created on April 3, 2017, 12:35 PM
 * 
 * The Menu class is used to embody a hierarchical menuing system. 
 * 
 */

#ifndef MENU_H
#define MENU_H


#include <cstddef>
#include <vector>
#include "MenuOperation.h"
#include "MenuOption.h"
#include "MenuTask.h"
#include "MenuKeyDuplicatedException.h"
#include "NotFoundException.h"


using namespace std;


const int MENU_SAME	    = 0;
const int MENU_UP	    = 1;


/**
 * The Menu class is used to contain a title, a prompt and a set (vector) of 
 * options that will appear on a text-based menu. 
 * 
 * The Menu class is extended from MenuOperation to enable the ability to put
 * sub-menus on menus.
 */
class Menu : public MenuOperation
{
private:
    string title;
    string prompt;

    int vSize;
    vector<MenuOption*> options;
    
    MenuOperation* displayMenu(void);

    void getUserEntry(void);

    MenuOption* getOptionForChar(const char moCh);

public:
    /**
     * Default constructor.
     */
    Menu();
    
    /**
     * Standard constructor that sets the title and prompt.
     * 
     * @param title
     * @param prompt
     */
    Menu(const string title, const string prompt);
    
    /**
     * Default destructor.
     */
    ~Menu();

 
    /**
     * Method used to add MenuOptions 'this' menu during menu creation.
     * Remember, MenuOptions can be standard MenuOptions or they can be Menus.
     * 
     * @param mo The MenuOption to be added.
     * @throws MenuKeyDuplicatedException if the MenuOption being added has
     * specified the a key that has already been specified by a previous 
     * MenuOption.
     */
    void addOption(MenuOption* mo) throw(MenuKeyDuplicatedException);

    /**
     * Method used to run the 'this' menu.
     * 
     * @return 
     */
    int run();
    
     

};

#endif /* MENU_H */

