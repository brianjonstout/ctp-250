#include "SearchByDate.h"

using namespace std;

void SearchByDate::visit(Film& film)
{
    int month = film.getMonth(); 

    if(this->getCond() == month)
    {
        //Only print once and if something is found
        if(this->getFound() == false)
        {
            film.printheader();
            this->setFound(true);
        }
        cout << film << endl;
    }
}