#include "Csv.h"

#include <iostream>
#include <string>
using namespace std;

Csv::Csv(string s)
{
    record = s;
}


Csv::Csv()
{

}


void Csv::setString(string s)
{
    record = s;
}


string Csv::getString()
{
    return record;
}


bool Csv::getNext(string& val)
{

    size_t pos = this->record.find_first_of(",", this->lastPos);
    if(pos == string::npos)
    {
        //cout << "length: " << this->record.length() << " lastPos: " << this->lastPos << endl;
        if(lastPos != this->record.length())
        {
            val = this->record.substr(lastPos, this->record.length() - lastPos);
            this->lastPos = this->record.length(); //Set the position of the previous comma
            return true;
        }
        return false;
    }
    else
    {
        val = this->record.substr(lastPos, pos-this->lastPos);
        this->lastPos = pos+1;
        return true;
    }
}

