
/**
 * @file MenuDBTask.h
 * @author Brian Stout (Modified), Daniel Hall
 * @brief Modified MenuTask which allows the passing through of a FilmDatabase object
 * @version 1.0
 * @date 2020-04-27
 */
#ifndef MENUDBTASK_H
#define MENUDBTASK_H

#include "MenuOperation.h"
#include "FilmDatabase.h"
#include "BinarySearchTree.h"
#include "BinaryNode.h"
#include "BinaryNodeTree.h"


/*
 * typedef necessary to allow passing MenuOperation function pointer.
 */
// https://isocpp.org/wiki/faq/pointers-to-members was a life saver for helping define
//  my own working function pointer to pass a DB pointer
typedef int (*MenuDBOpFPtr)(FilmDatabase*);


class MenuDBTask : public MenuOperation
{
private:
    int (*opToDo)(FilmDatabase*);
    FilmDatabase* filmDB;

public:
    /**
     * Constructor used to create a MenuTask with a particular function as the
     * the functionality to be performed when accessed.
     * 
     * @param opToDo
     */
    MenuDBTask(MenuDBOpFPtr, FilmDatabase* film); 

    ~MenuDBTask();
    
    /**
     * The run method will be called when the user selects the the MenuOption
     * that contains this MenuTask.
     * 
     * @return int success or failure.
     */
    int run(void);
};

#endif /* MENUDBTASK_H */