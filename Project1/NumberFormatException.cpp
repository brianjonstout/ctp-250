/* 
 * @File:   NumberFormatException.cpp
 * @Author: Daniel Hall
 *
 * Created on April 4, 2017, 11:45 AM
 *
 */

#include "NumberFormatException.h"  

NumberFormatException::NumberFormatException(const string& message)
         : logic_error("NumberFormatException: " + message)
{
}  // end constructor

// End of implementation file.

