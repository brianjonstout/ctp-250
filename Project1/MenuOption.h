/* 
 * @File:   MenuOption.h
 * Author: Daniel Hall
 *
 * Created on April 3, 2017, 12:35 PM
 */

#ifndef MENUOPTION_H
#define MENUOPTION_H

#include <cstdlib>
#include <iostream>
#include "MenuTask.h"
using namespace std;

/**
 * The MenuOption class embodies the data and functionality for a single 
 * menu option that will appear as an entry on/in a Menu object.
 * 
 * Every MenuOption has a char key that is presented to the user to activate
 * the functionality associated with the MenuOption.
 * 
 * Every MenuOption has a label that is use to provide description of the 
 * menu option. 
 * 
 * Every MenuOption has a MenuOperation that is used to embody the functionality
 * that should be executed when the user selects the option by pressing the key.
 */
class MenuOption {
    
private:
    char    key;
    string  label;    
    MenuOperation*    mOp;
    
public:
    
    /**
     * Constructor that creates the MenuOption with the various properties.
     * 
     * @param key The char key that will be used to activate the option.
     * @param label The label display to the user describing the option.
     * @param mOp The MenuOption containing the functionality to be executed
     * when the user selects the option.
     */
    MenuOption(const char key, const string& label, MenuOperation* mOp);
    
    /**
     * Default destructor.
     */
    ~MenuOption();
    
    /**
     * Returns the char key associated with the option.
     * @return char key.
     */
    const char getKey();
    
    /**
     * Returns the label associated with the option.
     * @return string label.
     */
    const string getLabel();
    
    /**
     * 
     * @param rightHandSide
     * @return 
     */
    bool operator==(const char& rightHandSide);
    
    /**
     * Returns the MenuOperation associated with this option.
     * @return MenuOption.
     */
    MenuOperation* getOperation(void);  
};

#endif /* MENUOPTION_H */

