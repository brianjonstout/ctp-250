/**
 * @file SearchByKeyword.h
 * @author Brian Stout
 * @brief Prints out any film which matches any keyword provided by in a vector
 * @version 1.0
 * @date 2020-04-28
 * 
 */
#pragma once

#include <iostream>
#include <vector>
#include "ConditionalVisit.h"
#include "Utils.h"
#include "FilmDatabase.h"

using namespace std;

/**
 * @brief Prints out any film which matches any keyword provided by in a vector
 * 
 */
class SearchByKeyword: public ConditionalVisit<vector<string>>
{
    public:
        void visit(Film&);

};