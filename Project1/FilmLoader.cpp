#include "FilmLoader.h"

using namespace std;

FilmLoader::FilmLoader(const char* csvFileName)
{
    inFileName = csvFileName;
    reader.open(csvFileName);

    //TODO raise exception if file is not open
    if(reader.is_open() == false)
    {
        cout << "ERROR: File is not open" << endl;
        exit(1);
        //TODO:Raise Exception
    }
}

FilmLoader::~FilmLoader()
{
    reader.close();
}

bool FilmLoader::getNext(Film& film)
{
    string record = "";

    if(getline(reader, record))
    {
        film = Film(record);
        return true;
    }
    else
    {
        return false;
    }
    
}