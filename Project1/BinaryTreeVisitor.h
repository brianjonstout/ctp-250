/*
	BinaryTreeVisitor.h
	
	This class represents a base class to be extended
	in order to implement the functionality for any
	desired BinaryNodeTree visitor.
	
	Author:	Daniel Hall / CTP250 Instructor
	
*/


#ifndef _BINARY_TREE_VISITOR
#define _BINARY_TREE_VISITOR

#include "Film.h"


class BinaryTreeVisitor
{
public:    
    BinaryTreeVisitor();
    ~BinaryTreeVisitor();
    
    virtual void visit(Film& item) = 0;	
};

#endif