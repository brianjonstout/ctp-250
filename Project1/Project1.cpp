/**
 * @file Project1.cpp
 * @author Brian J. Stout
 * @brief Main file for project1
 * @version 1.0
 * @date 2020-04-28
 * 
 */
#include <iostream>
#include <iomanip>
#include <ctime>
#include <time.h>

#include "Csv.h"
#include "Film.h"
#include "FilmLoader.h"
#include "FilmDatabase.h"
#include "Menu.h"
#include "MenuOption.h"
#include "MenuTask.h"
#include "MenuDBTask.h"
#include "Utils.h"
#include "PromptUtils.h"


using namespace std;

void menuBuildAndRun(FilmDatabase *);

/**
 * @brief menu exit function
 * 
 * @return int for menu control
 */
int menuExit();

/**
 * @brief sub menu exit function
 * 
 * @return int for menu control
 */
int subMenuExit();

/**
 * @brief Print program which describes the program
 * 
 * @return int for menu control
 */
int describeTheProgram();

//Menu options for the various tasks for specifications
int orderByFilmTitle(FilmDatabase *);
int orderByRank(FilmDatabase *);

int searchTitle(FilmDatabase *);
int searchKeyword(FilmDatabase *);
int searchStudio(FilmDatabase *);
int searchMonthOfRelease(FilmDatabase *);

int main()
{
    FilmDatabase filmDB;

    // Loads up the database
    FilmLoader loader("Films2016.csv");
    Film film;
    while(loader.getNext(film))
    {
        filmDB.add(film);
    }

    // Run the menus
    menuBuildAndRun(&filmDB);
}

void menuBuildAndRun(FilmDatabase * filmDB)
{
    /* MENU-ING STARTS */

    //Create the main menu
    Menu menu("Main Menu", "Enter Selection ->");

    //Create describe the program menu option
    MenuTask describeTask(describeTheProgram);
    MenuOption optionDescribe('D', "\"Describe the Program\"", &describeTask);
    menu.addOption(&optionDescribe);

    //Create sub menu exit program
    MenuTask submenuExitTask(subMenuExit);
    MenuOption optionSubmenuExit('X', "\"Return to main menu\"",&submenuExitTask);

    //Create the reports sub menu
    Menu reportSubmenu("REPORTS MENU", "Enter Selection ->");

    //Create Order By Film Title menu option
    MenuDBTask orderByFilmTitleTask(orderByFilmTitle, filmDB);
    MenuOption optionOrderByFilmTitle('T', "\"Order by Film Title report\"", &orderByFilmTitleTask);
    reportSubmenu.addOption(&optionOrderByFilmTitle);

    //Create Order by Rank menu option
    MenuDBTask orderByRankTask(orderByRank, filmDB);
    MenuOption optionOrderByRank('R', "\"Order by Rank report\"", &orderByRankTask);
    reportSubmenu.addOption(&optionOrderByRank);

    //Add option to return to main menu
    reportSubmenu.addOption(&optionSubmenuExit);

    //Add reports sub menu to Main menu
    MenuOption optionReportSubmenu('R', "\"Reports\"", &reportSubmenu);
    menu.addOption(&optionReportSubmenu);

    //Create the search sub menu
    Menu searchSubmenu("SEARCH MENU", "Enter Selection ->");

    //Create search by title menu option
    MenuDBTask searchTitleTask(searchTitle, filmDB);
    MenuOption optionSearchTitle('T', "\"Search by Title\"", &searchTitleTask);
    searchSubmenu.addOption(&optionSearchTitle);

    //Create search by Keywords menu option
    MenuDBTask searchKeywordTask(searchKeyword, filmDB);
    MenuOption optionSearchKeyword('K', "\"Search by Keyword(s)\"", &searchKeywordTask);
    searchSubmenu.addOption(&optionSearchKeyword);

    //Create search by studio menu option
    MenuDBTask searchStudioTask(searchStudio, filmDB);
    MenuOption optionSearchStudio('S', "\"Search by Studio\"", &searchStudioTask);
    searchSubmenu.addOption(&optionSearchStudio);

    //Create search by month of release menu option
    MenuDBTask searchMonthOfReleaseTask(searchMonthOfRelease, filmDB);
    MenuOption optionSearchMonthOfRelease('M', "\"Search by month of release\"", &searchMonthOfReleaseTask);
    searchSubmenu.addOption(&optionSearchMonthOfRelease);

    //Add option to return to main menu
    searchSubmenu.addOption(&optionSubmenuExit);

    //Add search sub menu to Main menu
    MenuOption optionSearchSubmenu('S', "\"Search the Database\"", &searchSubmenu);
    menu.addOption(&optionSearchSubmenu);

    //Add the exit
    MenuTask exitTask(menuExit);
    MenuOption optionExit('X', "\"Exit the Program\"", &exitTask);
    menu.addOption(&optionExit);

    /* MENU-ING DONE */

    menu.run();
}

//TODO: move all menu functions into an auxiliary file
int menuExit()
{
    return MENU_UP;
}

int subMenuExit()
{
    return MENU_UP;
}

int describeTheProgram()
{
    cout    << "This program contains a database of film." << endl 
            << "Navigate through the menu by entering the letter of the corresponding option letter (Requires upper case)" << endl
            << "You can display all the movie titles in the D (display) section.  You can query for specific movies in the S (search) section" << endl;
    return MENU_SAME;
}

int orderByFilmTitle(FilmDatabase * filmDB)
{
    filmDB->displayData();
    return MENU_SAME;
}

int orderByRank(FilmDatabase * filmDB)
{
    filmDB->displayByRank();
    return MENU_SAME;
}

int searchTitle(FilmDatabase * filmDB)
{
    string input;
    cout << "Title: ";
    getline(cin, input);
    filmDB->searchByTitle(input);
    return MENU_SAME;
}

int searchKeyword(FilmDatabase * filmDB)
{
    string input;
    cout << "Keywords: ";
    getline(cin, input);
    filmDB->searchByKeyword(input);
    return MENU_SAME;
}

int searchStudio(FilmDatabase * filmDB)
{
    string input;
    cout << "Studio: ";
    getline(cin, input);
    filmDB->searchByStudio(input);
    return MENU_SAME;
}

int searchMonthOfRelease(FilmDatabase * filmDB)
{
    int result;
    result = promptForPositiveInt("Release Month [1-12]:", "Please enter a number from 1-12", 1, 12);
    cout << result;
    filmDB->searchByDate(result);
    return MENU_SAME;
}