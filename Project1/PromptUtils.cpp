/*
 * @File:	PromptUtils.h
 * @Author:	Daniel Hall
 * Course:	CTP-250
 * 
 * Header file for PromptUtils.cpp
 * 
 * Created on January 22, 2017, 5:49 PM
 *  
 * Note:    Function header documentation is presented in PromptUtils.h.
 */




#include "PromptUtils.h"

using namespace std;

/**
 
 */
int promptForPositiveInt(string prompt, string description, int min, int max) {
    
    const int INVALID = -1;
    string sVal;    
    int value = INVALID;
    
    while (value == INVALID) {
	cout << prompt;
	getline(cin, sVal);	// gets a c-string (instead of a string)


	// Need to ensure that the user entered a number and didn't just
	// hit return without entering anything.
	if (strlen(sVal.c_str()) == 0) {
	    cout << description << endl;
	    continue;
	}
	
	if (pfpiIsInt(sVal)) {
	    value = atoi(sVal.c_str());
	}
	if (value == INVALID) {
	    cout << description << endl;
	    continue;
	}
//	cout << "min = " << min << endl;
//	cout << "max = " << max << endl;
//	
//	if (min == -1) cout << "min is -1" << endl;
//	else cout << "min is not -1" << endl;
//	
//	if (max == -1) cout << "max is -1" << endl;
//	else cout << "max is not -1" << endl;
	
	
	if ((min != -1 && value < min) || (max != -1 && value > max)) {
	    char sMin[20], sMax[20];	    
	    sprintf(sMin, "%d",min);
	    sprintf(sMax, "%d", max);
		    
	    cout << "Value out of range [" 
		    << ((min  == -1)? "*" : sMin)
		    << "," 
		    << ((max  == -1)? "*" : sMax)
		    << "]" << endl
		    << description << endl;
	    value = INVALID;
	}
    }
    return value;

}

/**
 * Validates that the parameter s contains only digits.
 * 
 * @param s
 * @return bool True if parameter contains only digits, false otherwise.
 */ 
bool pfpiIsInt(const string s) {
    
    for (int i = 0; i < s.length(); i++) {
	if (!isdigit(s[i])) {
	    return false;
	}
    }
    return true;
}


char promptForYN(string prompt) {
    char ynVal = 'X';
    string sVal; 
    string restOfLine;
    char ch;
    
    cin.clear();
    
    while (ynVal != 'Y' && ynVal != 'N') {
	cout << prompt;
	getline(cin, sVal); // gets a c-string (instead of a string)
	cin.clear();
	
	if (!isalpha(sVal[0])) {
	    cout << "That's not a valid value." << endl;
	    continue;
	}
	ynVal = toupper(sVal[0]);
	if (ynVal == 'N' || ynVal == 'Y') {
	    return ynVal;
	}

	if (ynVal != 'Y' && ynVal != 'N') {
	    cout << "That's not a valid value." << endl;
	    continue;
	}	
    }	
}



char promptForCharInSet(const string prompt, const int vSize, const vector<char> allowableChars) {
    char inChar = 'X';
    string sVal; 
    string restOfLine;
    char ch;
    bool validChar = false;
    
    cin.clear();
    
    while (validChar == false) {
	cout << prompt;
	getline(cin, sVal); // gets a c-string (instead of a string)
	cin.clear();
	
	inChar = sVal[0];
	for (int i = 0; i < vSize; i++) {
	    if (inChar == allowableChars[i]) {
		return inChar;
	    }
	}	
	
	string set("[");
	for (int i = 0; i < vSize; i++) {
	    set = set + allowableChars[i];
	    if (i < (vSize-1)) {
		set = set + ",";
	    }
	}
	set = set + "]";
	cout << "Please enter a value " + set + " ";
    }	
}