#include "Utils.h"
#include <string>
#include <iostream>

const char * intToMonth[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

void Utils::lowerString(string& s)
{
    for(int i = 0; i < s.length(); i++)
    {
        s[i] = tolower(s[i]);
    }
}

string Utils::dateformat(int day, int month)
{
    if(month > 12 || month < 1)
    {
        cout << "Date: " << month << "Day: " << day << endl;
        return "INVALID";
    }
    else
    {
        return to_string(day) + "-" + intToMonth[month-1];
    }
    
}

int Utils::monthLetterToInt(string month)
{
    for(int i = 0; i < 12; i++)
    {
        if(month == intToMonth[i])
        {
            return i + 1;
        }
    }
    return -1;
}