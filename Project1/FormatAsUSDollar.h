/* 
 * File:   FormatAsUSDollar.h
 * Author: Daniel Hall
 *
 * Created on April 11, 2017, 12:29 PM
 */

#pragma once

#include <cstddef>
#include <iostream>
#include <iomanip>
#include <locale>
#include <cstring>
#include <cstdio>
#include <sstream>

using namespace std;

/**
 * Function is used to convert a double value into a properly formatted
 * monetary string value.
 * 
 * "34567.66" is converted to "$ 34,567.66"
 * 
 * @param d The double to be converted into a monetary string value.
 * @return A properly formatted monetary string value.
 */

namespace Format
{
    string formatAsUSMoney(double d);
}