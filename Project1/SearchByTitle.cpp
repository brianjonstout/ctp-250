#include "SearchByTitle.h"

using namespace std;

void SearchByTitle::visit(Film& film)
{
    string titleLower = film.getFilmTitle(); 
    Utils::lowerString(titleLower);

    if(this->getCond() == titleLower)
    {
        this->setReport(film);
        this->setFound(true);
    }
}