/* 
 * @File:   NumberFormatException.h
 * @Author: Daniel Hall
 *
 * Created on April 4, 2017, 11:45 AM
 *
 */

#ifndef _NUMBER_FORMAT_EXCEPTION
#define _NUMBER_FORMAT_EXCEPTION

#include <stdexcept>
#include <string>

using namespace std;

/**
 * Exception used to signal to a caller that numeric parsing has failed.
 * 
 * @param message
 */
class NumberFormatException : public logic_error
{
public:
   NumberFormatException(const string& message = "");
}; // end NumberFormatException 
#endif
