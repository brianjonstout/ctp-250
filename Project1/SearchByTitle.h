/**
 * @file SearchByTitle.h
 * @author Brian Stout
 * @brief Prints out any film with a matching title
 * @version 1.0
 * @date 2020-04-28
 * 
 */
#pragma once

#include <iostream>
#include "ConditionalVisit.h"
#include "Utils.h"

using namespace std;

/**
 * @brief Will find a match of a film title and display it
 * 
 */
class SearchByTitle: public ConditionalVisit<string>
{
    public:
        void visit(Film&);

};