/* 
 * @File:   MenuOperation.h
 * Author: Daniel Hall
 *
 * Created on April 4, 2017, 11:45 AM
 */

#ifndef MENUOPERATION_H
#define MENUOPERATION_H

/**
 * Base class for options that can appear as part of a menu. 
 */
class MenuOperation 
{
private:
    
public:
    
    /**
     * Default constructor.
     */
    MenuOperation();    
    
    /**
     * The run method is used to execute whatever functionality is tied to the
     * the menu option.
     * 
     * @return int - The value returned by the run method is used to indicate
     * whether the menuing system should stay/rerun the current menu, or pop
     * up to a parent menu.
     */
    virtual int run();    
};

#endif /* MENUOPERATION_H */

