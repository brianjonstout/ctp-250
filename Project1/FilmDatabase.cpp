#include "FilmDatabase.h"


using namespace std;

bool FilmDatabase::add(Film& film)
{
    return filmDatabaseBST.add(film);
}

bool FilmDatabase::remove(Film& film)
{
    return filmDatabaseBST.remove(film);
}

void FilmDatabase::displayData()
{
    Film printer;
    printer.printheader();
    DisplayVisitor display;
    filmDatabaseBST.inorderTraverse(display);
}

void FilmDatabase::displayByRank()
{
    DisplayByRank display;
    Film printer;
    printer.printheader();

    //Loop which searches a BST for all Ranks in the BST, if there are 100 films, there should be 100 ranks
    for(int i = 1; i <= filmDatabaseBST.getNumberOfNodes(); i++)
    {
        display.setCond(i);
        display.setFound(false);

        //Uses preorderConditionalTraverse which will stop traversing the BST when a match is found
        filmDatabaseBST.preorderConditionalTraverse(display);
        if(display.getFound())
        {
            cout << display.report << endl;
        }
    }

}

int FilmDatabase::getNumberOfFilms()
{
    return filmDatabaseBST.getNumberOfNodes();
}

//TODO redundant
void FilmDatabase::displayByTitle()
{
    this->displayData();
}

void FilmDatabase::searchByTitle(string title)
{
    SearchByTitle display;
    Utils::lowerString(title);
    display.setCond(title);
    filmDatabaseBST.preorderConditionalTraverse(display);

    // Only print the header once, and if an object was found
    if(display.getFound())
    {
        display.getReport().printheader();
        cout << display.getReport() << endl;
    }
    else
    {
        cout << "No results found!" << endl;
    }
    
}

void FilmDatabase::searchByKeyword(string keywords)
{
    SearchByKeyword display;
    Csv csv(keywords);
    
    string word;
    vector<string> keywordVector;

    //The keywords are comma delimited, so create a Vector of keywords to loop through later
    while(csv.getNext(word))
    {
        Utils::lowerString(word);
        keywordVector.push_back(word);
    }

    display.setCond(keywordVector);

    //Traverse through all films comparing each film title to the key words
    filmDatabaseBST.preorderTraverse(display);

    if(display.getFound() == false)
    {
        cout << "No results found!" << endl;
    }
    
}

void FilmDatabase::searchByStudio(string studio)
{
    SearchByStudio display;
    Utils::lowerString(studio);
    display.setCond(studio);
    filmDatabaseBST.inorderTraverse(display);

    if(display.getFound() == false)
    {
        cout << "No results found!" << endl;
    }
}

void FilmDatabase::searchByDate(int month)
{
    SearchByDate display;
    display.setCond(month);
    filmDatabaseBST.inorderTraverse(display);

    if(display.getFound() == false)
    {
        cout << "No results found!" << endl;
    }
}