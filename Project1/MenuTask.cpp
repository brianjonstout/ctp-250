/* 
 * @File:   MenuTask.cpp
 * @Author: Daniel Hall
 *
 * Created on April 4, 2017, 11:45 AM
 */

#include <cstdlib>
#include "MenuTask.h"



MenuTask::MenuTask(MenuOpFPtr opToDo){
    this->opToDo = opToDo;
}


MenuTask::~MenuTask() {
    
}


int MenuTask::run(void) {   
    return (*opToDo)();    
}
