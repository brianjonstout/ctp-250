//� Created by Frank M. Carrano and Timothy M. Henry.
//� Copyright (c) 2017 Pearson Education, Hoboken, New Jersey.

/** ADT bag: Link-based implementation.
 * Upodated to include the new methods: BagUnion, bagInterface, and Bag Difference
 * @author Brian J. Stout
 * @author Prof Hall (Code from class)
 * @author Frank M. Carrano
 * @author Timothy M. Henry
 * @file LinkedBag.cpp
 */

#include "LinkedBag.h"
#include "Node.h"
#include <cstddef>

void displayBag(LinkedBag<std::string>& bag);

template<class ItemType>
LinkedBag<ItemType>::LinkedBag() : headPtr(nullptr), itemCount(0)
{
}  // end default constructor

template<class ItemType>
LinkedBag<ItemType>::LinkedBag(const LinkedBag<ItemType>& aBag)
{
	itemCount = aBag.itemCount;
   Node<ItemType>* origChainPtr = aBag.headPtr;  // Points to nodes in original chain
   
   if (origChainPtr == nullptr)
      headPtr = nullptr;  // Original bag is empty
   else
   {
      // Copy first node
      headPtr = new Node<ItemType>();
      headPtr->setItem(origChainPtr->getItem());
      
      // Copy remaining nodes
      Node<ItemType>* newChainPtr = headPtr;      // Points to last node in new chain
      origChainPtr = origChainPtr->getNext();     // Advance original-chain pointer
      
      while (origChainPtr != nullptr)
      {
         // Get next item from original chain
         ItemType nextItem = origChainPtr->getItem();
              
         // Create a new node containing the next item
         Node<ItemType>* newNodePtr = new Node<ItemType>(nextItem);
         
         // Link new node to end of new chain
         newChainPtr->setNext(newNodePtr);
         
         // Advance pointer to new last node
         newChainPtr = newChainPtr->getNext();

         // Advance original-chain pointer
         origChainPtr = origChainPtr->getNext();
      }  // end while
      
      newChainPtr->setNext(nullptr);              // Flag end of chain
   }  // end if
}  // end copy constructor

template<class ItemType>
LinkedBag<ItemType>::~LinkedBag()
{
   clear();
}  // end destructor

template<class ItemType>
bool LinkedBag<ItemType>::isEmpty() const
{
	return itemCount == 0;
}  // end isEmpty

template<class ItemType>
int LinkedBag<ItemType>::getCurrentSize() const
{
	return itemCount;
}  // end getCurrentSize

template<class ItemType>
bool LinkedBag<ItemType>::add(const ItemType& newEntry)
{
   // Add to beginning of chain: new node references rest of chain;
   // (headPtr is null if chain is empty)        
   Node<ItemType>* nextNodePtr = new Node<ItemType>();
   nextNodePtr->setItem(newEntry);
   nextNodePtr->setNext(headPtr);  // New node points to chain
//   Node<ItemType>* nextNodePtr = new Node<ItemType>(newEntry, headPtr); // alternate code

   headPtr = nextNodePtr;          // New node is now first node
   itemCount++;
   
   return true;
}  // end add

template<class ItemType>
std::vector<ItemType> LinkedBag<ItemType>::toVector() const
{
   std::vector<ItemType> bagContents;
   Node<ItemType>* curPtr = headPtr;
   int counter = 0;
	while ((curPtr != nullptr) && (counter < itemCount))
   {
		bagContents.push_back(curPtr->getItem());
      curPtr = curPtr->getNext();
      counter++;
   }  // end while
   
   return bagContents;
}  // end toVector

template<class ItemType>
bool LinkedBag<ItemType>::remove(const ItemType& anEntry)
{
   Node<ItemType>* entryNodePtr = getPointerTo(anEntry);
   bool canRemoveItem = !isEmpty() && (entryNodePtr != nullptr);
   if (canRemoveItem)
   {
      // Copy data from first node to located node
      entryNodePtr->setItem(headPtr->getItem());
      
      // Delete first node
      Node<ItemType>* nodeToDeletePtr = headPtr;
      headPtr = headPtr->getNext();
      
      // Return node to the system
      nodeToDeletePtr->setNext(nullptr);
      delete nodeToDeletePtr;
      nodeToDeletePtr = nullptr;
      
      itemCount--;
   } // end if
   
	return canRemoveItem;
}  // end remove

template<class ItemType>
void LinkedBag<ItemType>::clear()
{
   Node<ItemType>* nodeToDeletePtr = headPtr;
   while (headPtr != nullptr)
   {
      headPtr = headPtr->getNext();

      // Return node to the system
      nodeToDeletePtr->setNext(nullptr);
      delete nodeToDeletePtr;
      
      nodeToDeletePtr = headPtr;
   }  // end while
   // headPtr is nullptr; nodeToDeletePtr is nullptr
   
	itemCount = 0;
}  // end clear

template<class ItemType>
int LinkedBag<ItemType>::getFrequencyOf(const ItemType& anEntry) const
{
	int frequency = 0;
   int counter = 0;
   Node<ItemType>* curPtr = headPtr;
   while ((curPtr != nullptr) && (counter < itemCount))
   {
      if (anEntry == curPtr->getItem())
      {
         frequency++;
      } // end if
      
      counter++;
      curPtr = curPtr->getNext();
   } // end while
   
	return frequency;
}  // end getFrequencyOf

template<class ItemType>
bool LinkedBag<ItemType>::contains(const ItemType& anEntry) const
{
	return (getPointerTo(anEntry) != nullptr);
}  // end contains

/* ALTERNATE 1
template<class ItemType>
bool LinkedBag<ItemType>::contains(const ItemType& anEntry) const
{
   return getFrequencyOf(anEntry) > 0;
} 
*/
/* ALTERNATE 2 
template<class ItemType>
bool LinkedBag<ItemType>::contains(const ItemType& anEntry) const
{
   bool found = false;
   Node<ItemType>* curPtr = headPtr;
   int i = 0;
   while (!found && (curPtr != nullptr) && (i < itemCount))
   {
      if (anEntry == curPtr-<getItem())
      {
         found = true;
      }
      else
      {
         i++;
         curPtr = curPtr->getNext();
      }  // end if
   }  // end while

   return found;
}  // end contains
*/

// private
// Returns either a pointer to the node containing a given entry 
// or the null pointer if the entry is not in the bag.
template<class ItemType>
Node<ItemType>* LinkedBag<ItemType>::getPointerTo(const ItemType& anEntry) const
{
   bool found = false;
   Node<ItemType>* curPtr = headPtr;
   
   while (!found && (curPtr != nullptr))
   {
      if (anEntry == curPtr->getItem())
         found = true;
      else
         curPtr = curPtr->getNext();
   } // end while
   
   return curPtr;
} // end getPointerTo

//END OF GIVEN CODE
//START OF STUDENT CODE


/**
 * combines two bags, it treats each number as an individual member, even if they're equivalent
 * @param otherBag The bag being combined with this
 * @return A bag with two combined sets
 */
template<class ItemType>
LinkedBag<ItemType> LinkedBag<ItemType>::bagUnion(const LinkedBag<ItemType> &otherBag) const
{

   if(this->itemCount > 0)
   {
      LinkedBag<ItemType> retBag(*this);
      if(otherBag.itemCount < 1)
      {
         return retBag;
      }

      Node<ItemType>* ptr = otherBag.headPtr;
      bool success = true;

      while(ptr != nullptr && success == true)
      {
         ItemType item = ptr->getItem();
         success = retBag.add(item);

         ptr = ptr->getNext();
      }

      return retBag;
   }
   else if (otherBag.itemCount > 0)
   {
      LinkedBag<ItemType> otherBagClone(otherBag);
      return otherBagClone;
   }
   else
   {
      LinkedBag<ItemType> retEmptyBag;
      return retEmptyBag;
   }

}


/**
 * creates a new bag with members which were contained in both sets.  If a memeber appears twice in a bag, it'll appear twice in the return bag
 * @param otherBag The bag being intersected with this
 * @return A bag with intersected sets
 */
template<class ItemType>
LinkedBag<ItemType> LinkedBag<ItemType>::bagIntersection(const LinkedBag<ItemType> &otherBag) const
{
   LinkedBag<ItemType> retBag;
   if(this->itemCount < 1 || otherBag.itemCount < 1)
   {
      return retBag;
   }

   retBag.headPtr = nullptr;
   bool takenFlag[otherBag.itemCount] = {false};

   Node<ItemType>* ptr = this->headPtr;
   Node<ItemType>* otherPtr = otherBag.headPtr;

   while(ptr != nullptr)
   {
      int i = 0;
      Node<ItemType>* otherPtr = otherBag.headPtr;
      while(otherPtr != nullptr)
      {
         if(takenFlag[i])
         {
            i++;
            otherPtr = otherPtr->getNext();
            continue;
         }
         if(ptr->getItem() == otherPtr->getItem())
         {
            ItemType item = otherPtr->getItem();
            retBag.add(item);
            takenFlag[i] = true;
            break;
         }
         i++;
         otherPtr = otherPtr->getNext();
      }
      ptr = ptr->getNext();
   }

   return retBag;
}


/**
 * Subtracts one bag from another, each memeber of the otherBag set will count once towards This set
 * @param otherBag The bag which will use to subtract
 * @return A bag containg no memebers of the set from otherBag
 */
template<class ItemType>
LinkedBag<ItemType> LinkedBag<ItemType>::bagDifference(const LinkedBag<ItemType> &otherBag) const
{
   LinkedBag<ItemType> retBag(*this);
   LinkedBag<ItemType> intBag = this->bagIntersection(otherBag);

   bool remove[retBag.itemCount] = {false};
   bool takenFlag[intBag.itemCount] = {false};

   while(true)
   {
      Node<ItemType>* ptr = retBag.headPtr;
      Node<ItemType>* otherPtr = intBag.headPtr;

      bool found = false;

      while(ptr != nullptr)
      {
         ItemType item = ptr->getItem();

         while(otherPtr != nullptr && intBag.itemCount > 0)
         {
            if(item == otherPtr->getItem())
            {
               intBag.remove(item);
               retBag.remove(item);
               found = true;
               break;
            }
            otherPtr = otherPtr->getNext();
         }
         //Chain the breaks to start the loop all over again
         if(found)
         {
            break;
         }
         otherPtr = intBag.headPtr;
         ptr = ptr->getNext();
      }

      //Break the infinite loop if ptr retBag linked List reaches end without finding a match or intBag is empty
      if(ptr == nullptr || intBag.itemCount == 0)
      {
         break;
      }
   }
   return retBag;
}


/**
 * returns true if the set is the same size, and contains the same memebers as otherBag
 * @param otherBag The bag being compared to
 * @return A true or false value representing if a bag is equivalent
 */
template<class ItemType>
bool LinkedBag<ItemType>::bagEquiv(const LinkedBag<ItemType> &bag) const
{
   LinkedBag<ItemType> retBag = this->bagDifference(bag);
   if(this->itemCount != bag.itemCount)
   {
      return false;
   }

   if(retBag.itemCount == 0)
   {
      return true;
   }
   else
   {
      return false;
   }	
   return false;
}


// CODE FROM CLASS
template<class ItemType>
void LinkedBag<ItemType>::displayBag()
{
   LinkedBag<ItemType>bag = *this;

   std::cout << "The bag contains " << bag.getCurrentSize()
   << " items:" << std::endl;
   std::vector<ItemType> bagItems = bag.toVector();
   
   int numberOfEntries = (int) bagItems.size();
   for (int i = 0; i < numberOfEntries; i++)
   {
      std::cout << bagItems[i] << " ";
   }  // end for
   std::cout << std::endl << std::endl;
}  // end displayBag



