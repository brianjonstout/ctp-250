/** BagTester for Lab4
  * @author Brian J Stout
  */

#include <iostream>
#include <string>
#include "LinkedBag.h"

using namespace std;

bool testEquiv();
bool testUnion();
bool testIntersection();
bool testDifference();

int main()
{
	bool testResults = true;
	bool retResults = false;

	retResults = testEquiv();
	if(retResults == false)
	{
		testResults = false;
	}

	retResults = testUnion();
	if(retResults == false)
	{
		testResults = false;
	}

	retResults = testIntersection();
	if(retResults == false)
	{
		testResults = false;
	}

	retResults = testDifference();
	if(retResults == false)
	{
		testResults = false;
	}

	if(testResults)
	{
		cout << "Yay!  All the tests passed" << endl;
	}
	else
	{
		cout << "Some tests failed!  What went wrong?" << endl;
	}

   	return 0;
}  

bool testEquiv()
{
	bool testResult = true;

	cout << "TESTING EQUIVALENCY TO SELF" << endl;
	LinkedBag<int> bag1;
	bag1.add(0);
	bag1.add(1);
	bag1.add(2);
	bag1.add(3);

	if(bag1.bagEquiv(bag1))
	{
		cout << "\tSubtest : Pass" << endl << endl;
	}
	else
	{
		testResult = false;
		cout << "\tSubtest : Fail" << endl << endl;
	}

	cout << "Testing equivalency to other true" << endl;
	LinkedBag<int> bag2;
	bag2.add(0);
	bag2.add(1);
	bag2.add(2);
	bag2.add(3);

   cout << "Bag 1 --" << endl;
   bag1.displayBag();
   cout << "Bag 2 --" << endl;
   bag2.displayBag();

	if(bag1.bagEquiv(bag2))
	{
		cout << "\tSubtest : Pass" << endl << endl;
	}
	else
	{
		testResult = false;
		cout << "\tSubtest : Fail" << endl << endl;
	}

	cout << "TESTING EQUIVALENCY TO NON EQUAL BAGS (bag1 & bag2, bag1 & bag3, bag1 & emptyBag)" << endl;
   cout << "Bag 1 --" << endl;
   bag1.displayBag();

	bag2.add(4);
   cout << "Bag 2 --" << endl;
   bag2.displayBag();

	LinkedBag<int> bag3;
	bag3.add(13);
   cout << "Bag 3 --" << endl;
   bag3.displayBag();

	LinkedBag<int> emptyBag;


	if(bag1.bagEquiv(bag2) || bag1.bagEquiv(bag3) || bag1.bagEquiv(emptyBag))
	{
		testResult = false;
		cout << "\tSubtest : Fail" << endl << endl;
	}
	else
	{
		cout << "\tSubtest : Pass" << endl << endl;
	}

	return testResult;
	

}

bool testUnion()
{
	bool testResult = true;

	cout << "TESTING UNION" << endl;

	LinkedBag<int> bag1;
	bag1.add(0);
	bag1.add(1);
	bag1.add(2);
   cout << "Bag1 --" << endl;
   bag1.displayBag();

	LinkedBag<int> bag2;
	bag2.add(3);
	bag2.add(4);
	bag2.add(5);
   cout << "Bag2 --" << endl;
   bag2.displayBag();

	LinkedBag<int> equivBag;
	equivBag.add(0);
	equivBag.add(1);
	equivBag.add(2);
	equivBag.add(3);
	equivBag.add(4);
	equivBag.add(5);
   cout << "Expected Results --" << endl;
   equivBag.displayBag();

   cout << "Result Bag --" << endl;
   LinkedBag<int> resultBag = bag1.bagUnion(bag2);
   resultBag.displayBag();
	if(equivBag.bagEquiv(resultBag))
	{
		cout << "\tSubtest : Pass" << endl << endl;
	}
	else
	{
		testResult = false;
		cout << "\tSubtest : Fail" << endl << endl;
	}

	return testResult;

}

bool testIntersection()
{
	bool testResult = true;

	LinkedBag<int> bag1;
	bag1.add(0);
	bag1.add(1);
	bag1.add(2);
	bag1.add(3);

	LinkedBag<int> bag2;
	bag2.add(3);
	bag2.add(4);
	bag2.add(5);
	bag2.add(6);

	LinkedBag<int> test1;
	test1.add(3);

	cout << "TESTING BASIC INTERSECTION" << endl;
   cout << "bag 1 --" << endl;
   bag1.displayBag();
   cout << "bag 2 --" << endl;
   bag2.displayBag();

   LinkedBag<int> resultBag = bag1.bagIntersection(bag2);
   cout << "result bag --" << endl;
   resultBag.displayBag();

   cout << "Expected Results -- " << endl;
   test1.displayBag();

	if(test1.bagEquiv(resultBag))
	{
		cout << "\tSubtest : Pass" << endl << endl;
	}
	else
	{
		testResult = false;
		cout << "\tSubtest : Fail" << endl << endl;
	}

	cout << "TESTING BASIC INTERSECTION (No common)" << endl;
	bag2.remove(3);

   cout << "Bag 1 --" << endl;
   bag1.displayBag();

   cout << "Bag 2 --" << endl;
   bag2.displayBag();

   resultBag = bag1.bagIntersection(bag2);
   cout << "Result Bag -- " << endl;
   resultBag.displayBag();

	if(resultBag.isEmpty())
	{
		cout << "\tSubtest : Pass" << endl << endl;
	}
	else
	{
		testResult = false;
		cout << "\tSubtest : Fail" << endl << endl;
	}
	
   cout << "TESTING MULTIPLES" << endl;
	bag1.add(3);
	bag1.add(3);
	bag2.add(3);
	bag2.add(3);
	bag2.add(3);
   bag2.add(4);
	test1.add(3);
   test1.add(3);

   cout << "Bag 1 --" << endl;
   bag1.displayBag();

   cout << "Bag 2 --" << endl;
   bag2.displayBag();

   resultBag = bag1.bagIntersection(bag2);
   cout << "Results bag --" << endl;
   resultBag.displayBag();

   cout << "Expected Results--" << endl;
   test1.displayBag();


	if(test1.bagEquiv(bag1.bagIntersection(bag2)))
	{
		cout << "\tSubtest : Pass" << endl << endl;
	}
	else
	{
		testResult = false;
		cout << "\tSubtest : Fail" << endl << endl;
	}

	return testResult;
}

bool testDifference()
{
	bool testResult = true;

	cout << "TESTING DIFFERENCE" << endl;
	LinkedBag<int> bag1;
	bag1.add(0);
	bag1.add(1);
	bag1.add(2);
	bag1.add(3);

	LinkedBag<int> bag2;
	bag2.add(3);
	bag2.add(4);
	bag2.add(5);
	bag2.add(6);

	LinkedBag<int> test1;
	test1.add(0);
	test1.add(1);
	test1.add(2);

   cout << "bag 1 --" << endl;
   bag1.displayBag();

   cout << "bag 2 --" << endl;
   bag2.displayBag();

   LinkedBag<int> resultBag = bag1.bagDifference(bag2);
   cout << "Results Bag" << endl;
   resultBag.displayBag();

   cout << "Expected Results" << endl;
   test1.displayBag();

	if(test1.bagEquiv(resultBag))
	{
		cout << "\tSubtest : Pass" << endl << endl;
	}
	else
	{
		testResult = false;
		cout << "\tSubtest : Fail" << endl << endl;
	}

	test1.add(3);
	LinkedBag<int> emptyBag;
	cout << "TESTING DIFFERENCE (empty bag) " << endl;
	if(test1.bagEquiv(bag1.bagDifference(emptyBag)))
	{
		cout << "\tSubtest : Pass" << endl << endl;
	}
	else
	{
		testResult = false;
		cout << "\tSubtest : Fail" << endl;
	}

	return testResult;
}